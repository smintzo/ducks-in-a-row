__author__ = 'steve'

import sys, pygame, random
pygame.init()

#             R    G    B
WHITE     = (255, 255, 255)
BLACK     = (  0,   0,   0)
RED       = (255,   0,   0)
GREEN     = (  0, 255,   0)
DARKGREEN = (  0, 155,   0)
GRAY      = ( 40,  40,  40)

NUM_ANIMALS = 6
MAX_DISAPPEAR = 3
MAX_DROP = 3

cell_size = 64
grid_w, grid_h = 15, 10
grid_size = width, height = cell_size * grid_w, cell_size * grid_h
score_size = score_width, score_height = cell_size * 2, cell_size * grid_h
screen_size = screen_width, screen_height = cell_size * (grid_w + 2), cell_size * grid_h

screen = pygame.display.set_mode(screen_size)
grid_rect = pygame.Rect(cell_size * 2, 0, width, height)
grid_surf = screen.subsurface(grid_rect)
score_rect = pygame.Rect(0, 0, score_width, score_height)
score_surf = screen.subsurface(score_rect)

font = pygame.font.Font(None, 36)

#grid is a rectangular array of triplets: the animal id (or -1 for empty),
#a disappear value (0 for undisappeared, 1 to MAX_DISAPPEAR to disappear the tile in increments),
#and a drop value (0 or undropped, 1 to MAX_DROP to drop the tile in increments).
grid = [[[random.randint(0, NUM_ANIMALS - 1), 0, 0] for y in range(grid_h)] for x in range(grid_w)]

UNSELECTED = [-1, -1]

current_cell = [0, 0]
selected_cell = UNSELECTED

FPS = 60
FPSCLOCK = pygame.time.Clock()

duck = pygame.image.load("duck.png")
puppy = pygame.image.load("puppy.png")
sheep = pygame.image.load("sheep.png")
bunny = pygame.image.load("bunny.png")
elephant = pygame.image.load("elephant.png")
monkey = pygame.image.load("monkey.png")

animals = [duck, puppy, sheep, bunny, elephant, monkey]
animal_counts = []

def main():
    global current_cell, selected_cell, animal_counts

    animal_counts = []
    for animal in animals: animal_counts.append(0)

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if selected_cell == UNSELECTED:
                    if event.key == pygame.K_LEFT and current_cell[0] > 0:
                        current_cell[0] = current_cell[0] - 1
                    if event.key == pygame.K_UP and current_cell[1] > 0:
                        current_cell[1] = current_cell[1] - 1
                    if event.key == pygame.K_RIGHT and current_cell[0] < grid_w - 1:
                        current_cell[0] = current_cell[0] + 1
                    if event.key == pygame.K_DOWN and current_cell[1] < grid_h - 1:
                        current_cell[1] = current_cell[1] + 1
                    if event.key == pygame.K_SPACE and selected_cell == UNSELECTED:
                        selected_cell = current_cell[:]
                else:
                    if event.key == pygame.K_LEFT and current_cell[0] > 0:
                        grid_swap(current_cell[0], current_cell[1], current_cell[0] - 1, current_cell[1])
                        selected_cell = UNSELECTED
                    if event.key == pygame.K_UP and current_cell[1] > 0:
                        grid_swap(current_cell[0], current_cell[1], current_cell[0], current_cell[1] - 1)
                        selected_cell = UNSELECTED
                    if event.key == pygame.K_RIGHT and current_cell[0] < grid_w - 1:
                        grid_swap(current_cell[0], current_cell[1], current_cell[0] + 1, current_cell[1])
                        selected_cell = UNSELECTED
                    if event.key == pygame.K_DOWN and current_cell[1] < grid_h - 1:
                        grid_swap(current_cell[0], current_cell[1], current_cell[0], current_cell[1] + 1)
                        selected_cell = UNSELECTED
                    if event.key == pygame.K_SPACE:
                        selected_cell = UNSELECTED

        anything_to_disappear = True
        anything_to_drop = True

        while anything_to_disappear or anything_to_drop:
            while anything_to_drop:

                #Find cells to drop
                anything_to_drop = False
                for x in range(grid_w):
                    for y in range(grid_h - 1):
                        if grid[x][y+1][0] == -1:
                            grid[x][y][2] = 1
                            anything_to_drop = True

                #Drop them
                if anything_to_drop:
                    for drop_step in range(2, MAX_DROP + 1):
                        draw_grid()
                        pygame.display.update()
                        for x in range(grid_w):
                            for y in range(grid_h-1):
                                if grid[x][y][2] > 0:
                                    if drop_step < MAX_DROP:
                                        grid[x][y][2] = grid[x][y][2] + 1
                                    else:
                                        grid[x][y+1] = [grid[x][y][0], 0, 0]
                                        grid[x][y] = [-1, 0, 0]

                #Find any empty cells in the top row
                for x in range(grid_w):
                    if grid[x][0][0] == -1:
                        grid[x][0] = [random.randint(0, NUM_ANIMALS - 1), 0, 0]

            #Find threes-in-a-row to be "disappeared"
            anything_to_disappear = False
            for x in range(grid_w):
                for y in range(grid_h):
                    if ((x > 1 and grid[x][y][0] == grid[x-1][y][0] == grid[x-2][y][0] > -1) or
                        (x > 0 and x < grid_w - 1 and grid[x][y][0] == grid[x-1][y][0] == grid[x+1][y][0] > -1) or
                        (x < grid_w - 2 and grid[x][y][0] == grid[x+1][y][0] == grid[x+2][y][0] > -1) or
                        (y > 1 and grid[x][y][0] == grid[x][y-1][0] == grid[x][y-2][0] > -1) or
                        (y > 0 and y < grid_h - 1 and grid[x][y][0] == grid[x][y-1][0] == grid[x][y+1][0] > -1) or
                        (y < grid_h - 2 and grid[x][y][0] == grid[x][y+1][0] == grid[x][y+2][0] > -1)):

                        grid[x][y][1] = 1
                        animal_counts[grid[x][y][0]] = animal_counts[grid[x][y][0]] + 1
                        anything_to_disappear = True

            #Disappear them ...
            if anything_to_disappear:
                for disappear_step in range(2, MAX_DISAPPEAR + 1):
                    draw_grid()
                    pygame.display.update()
                    for x in range(grid_w):
                        for y in range(grid_h):
                            if grid[x][y][1] > 0:
                                if disappear_step < MAX_DISAPPEAR:
                                    grid[x][y][1] = grid[x][y][1] + 1
                                else:
                                    grid[x][y] = [-1, 0, 0]

        draw_grid()

        pygame.display.update()
        FPSCLOCK.tick(FPS)

def draw_grid():
    global current_cell, selected_cell, animal_counts

    grid_surf.fill(DARKGREEN)

    for x in range(grid_w):
        for y in range(grid_h):
            cell = grid[x][y]
            cell_centre_x, cell_centre_y = (x + 0.5) * cell_size, (y + 0.5 + (cell[2] / float(MAX_DROP))) * cell_size
            this_cell_size = cell_size * (MAX_DISAPPEAR - cell[1]) / float(MAX_DISAPPEAR)
            cell_rect = pygame.Rect(cell_centre_x - this_cell_size / 2, cell_centre_y - this_cell_size / 2,
                                    this_cell_size, this_cell_size)
            if [x, y] == selected_cell:
                pygame.draw.rect(grid_surf, GREEN, cell_rect)

            if cell[0] >= 0:
                animal = animals[cell[0]]
                grid_surf.blit(pygame.transform.scale(animal, (int(this_cell_size), int(this_cell_size))), cell_rect)
                pygame.draw.rect(grid_surf, GRAY, cell_rect, 1)

            if [x, y] == current_cell:
                pygame.draw.rect(grid_surf, WHITE, cell_rect, 5)

    score_surf.fill(GRAY)

    for i in range(len(animals)):
        score_surf.blit(pygame.transform.scale(animals[i], (48, 48)), pygame.Rect(8, 64 * i + 8, 48, 48))
        text = font.render(str(animal_counts[i]), 0, WHITE)
        score_surf.blit(text, pygame.Rect(72, 64 * i + 8, 48, 48))

def grid_swap(x , y, new_x, new_y):
    global grid
    grid[x][y], grid[new_x][new_y] = grid[new_x][new_y], grid[x][y]

main()