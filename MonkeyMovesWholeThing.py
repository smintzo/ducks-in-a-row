import pygame, sys
from pygame.locals import *

pygame.init()

#******************
# Set Up the Screen
#******************

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)

# set up the window
windowWidth=640
windowHeight=512

DISPLAYSURF = pygame.display.set_mode((windowWidth, windowHeight), 0, 32)
pygame.display.set_caption('Monkey Moves')

#Set the Background colour
BACKGROUND=GREEN

# draw on the surface object
DISPLAYSURF.fill(BACKGROUND)

#*******************
# Set Up the Animals
#*******************

#Load the graphics
duckPic = pygame.image.load("duck.png")
puppyPic = pygame.image.load("puppy.png")
sheepPic = pygame.image.load("sheep.png")
bunnyPic = pygame.image.load("bunny.png")
elephantPic = pygame.image.load("elephant.png")
monkeyPic = pygame.image.load("monkey.png")

#******************
# Set Up the Player
#******************

playerPosX=320
playerPosY=200
playerSpeedX=7
playerSpeedY=7
playerSize=100
playerPicture= monkeyPic

up=pygame.K_UP
down=pygame.K_DOWN
left=pygame.K_LEFT
right=pygame.K_RIGHT

#******************
#The Main Game Loop
#******************

#Set the exit to allow us to escape
def terminate():
    pygame.quit()
    sys.exit()

#Set the speed of the game
FPS=30

while True:
    
    FPSCLOCK = pygame.time.Clock()

    for event in pygame.event.get():
        if event.type == QUIT:
             terminate()
        elif event.type == KEYDOWN:
             if event.key == K_ESCAPE:
                 terminate() 

    #***************
    #Move the player
    #***************

    #Erase previous image of player
    playerRect = pygame.Rect(playerPosX,playerPosY,playerSize,playerSize)
    pygame.draw.rect(DISPLAYSURF, BACKGROUND, playerRect, 0)


    #Check for key press and move the player
    keyPressed = pygame.key.get_pressed()


    #Standard Movement

##    if keyPressed[left]:
##        playerPosX -= playerSpeedX
## 
##    if keyPressed[right]:
##        playerPosX += playerSpeedX
##        
##    if keyPressed[up]:
##        playerPosY -= playerSpeedY
##        
##    if keyPressed[down]:
##        playerPosY += playerSpeedY


##    #Accelerator Monkey


##    if keyPressed[left]:
##        playerSpeedX -=1
## 
##    if keyPressed[right]:
##        playerSpeedX +=1
##
##    playerPosX += playerSpeedX
##        
##    if keyPressed[up]:
##        playerSpeedY -=1
##        
##    if keyPressed[down]:
##        playerSpeedY +=1
##
##    playerPosY += playerSpeedY


##  Flappy Monkey

    if keyPressed[left]:
        playerSpeedX -=1
 
    if keyPressed[right]:
        playerSpeedX +=1

    playerPosX += playerSpeedX

    if keyPressed[up]:
        playerPosY -= playerSpeedY
    else:
        playerPosY += playerSpeedY


    #*****************
    # Wrap around code
    #*****************

    if playerPosX > windowWidth:
        playerPosX = 0 - playerSize
    if playerPosX + playerSize < 0:
        playerPosX = windowWidth

    if playerPosY > windowHeight:
        playerPosY=0 - playerSize
    if playerPosY + playerSize < 0:
        playerPosY = windowHeight


    #*****************
    # Non around code
    #*****************
##
##    if playerPosX + playerSize > windowWidth:
##        playerPosX = windowWidth - playerSize
##        
##    if playerPosX < 0:
##        playerPosX = 0
##
##    if playerPosY + playerSize > windowHeight:
##        playerPosY=windowHeight-playerSize
##        
##    if playerPosY < 0:
##        playerPosY = 0 
   

    #Draw Player at the new position
    playerRect = pygame.Rect(playerPosX,playerPosY,playerSize,playerSize)
    DISPLAYSURF.blit(pygame.transform.scale(playerPicture, (playerSize, playerSize)), playerRect)

    #*******************
    #Re-Draw the display
    #*******************

    pygame.display.update()
    FPSCLOCK.tick(FPS)


